#!/bin/bash
setenforce 0
Install_Docker()
{
echo "Choose your OS as:AmazonLinux,Centos,RHEL"
read OS
if [[ ${OS} == "AmazonLinux" || ${OS} == "Centos" ]]
then
yum install docker -y
service docker start
else
dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
dnf list docker-ce
dnf install docker-ce --nobest -y
systemctl start docker
systemctl enable docker
fi
echo "################"
}
Install_Kubernetes()
{
#adding IP address to hosts
echo "$(hostname -I) k8s-master" >> /etc/hosts
#Adding kubernetes repo
cat >> /etc/yum.repos.d/kubernetes.repo <<EOL
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOL
#install kubeadm kubectl,kubelet
yum install -y kubeadm 

#enable services

systemctl start docker && systemctl enable docker
systemctl start kubelet && systemctl enable kubelet

#vi /etc/fstab
swapoff -a
#----------------------------------------
cat >> /etc/sysctl.conf <<EOL

net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOL
#----------------------------------------------

sysctl -p

sudo kubeadm init --pod-network-cidr=172.30.0.0/16 

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
}

Install_Docker
Install_Kubernetes

